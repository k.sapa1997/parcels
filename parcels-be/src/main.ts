import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { GlobalExceptionHandler } from './middleware/global-exceptions-filter';
import * as fs from 'fs';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Test api')
    .setDescription('Here would be a description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  fs.writeFileSync('./openapi.json', JSON.stringify(document));

  app.useGlobalPipes(new ValidationPipe());

  app.enableCors({ origin: true, credentials: true });
  app.useGlobalFilters(new GlobalExceptionHandler());

  await app.listen(3000);
}
bootstrap();
