import { Body, Controller, Get, HttpStatus, Post, Query } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CreateParcelDTO, ParcelDto, GetAllQueryDTO } from './dto';
import { ParcelService } from './parcel.service';

@Controller('parcels')
@ApiTags('parcels')
export class ParcelController {
  constructor(private service: ParcelService) {}
  @Get()
  @ApiOkResponse({ type: ParcelDto, isArray: true })
  async getAll(@Query() query: GetAllQueryDTO): Promise<ParcelDto[]> {
    const res = await this.service.getAll(query);
    return res.map((it) => ParcelDto.fromEntity(it));
  }

  @Post()
  @ApiOkResponse({ type: ParcelDto, status: HttpStatus.CREATED })
  async create(@Body() dto: CreateParcelDTO): Promise<ParcelDto> {
    const res = await this.service.create(dto);
    return ParcelDto.fromEntity(res);
  }
}
