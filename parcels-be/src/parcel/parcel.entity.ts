import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../abstract/base.entity';
@Entity('parcels')
export class Parcel extends BaseEntity {
  @Column({ unique: true })
  SKU: string;

  @Column()
  description: string;

  @Column()
  country: string;

  @Column()
  town: string;

  @Column()
  street: string;

  @Column()
  deliveryDate: Date;
}
