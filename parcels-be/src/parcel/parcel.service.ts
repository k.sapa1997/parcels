import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Parcel } from './parcel.entity';
import { Repository } from 'typeorm';
import { CreateParcelDTO } from './dto';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { ParcelAddedEvent } from '../events/parcel-added.event';

export interface ParcelFindFilters {
  country?: string;
  description?: string;
}

@Injectable()
export class ParcelService {
  constructor(
    @InjectRepository(Parcel)
    private repository: Repository<Parcel>,
    private eventEmitter: EventEmitter2,
  ) {}

  async getAll(filters?: ParcelFindFilters): Promise<Parcel[]> {
    const query = this.repository.createQueryBuilder('parcel');

    if (filters?.country) {
      query.andWhere('parcel.country=:country', { country: filters?.country });
    }

    if (filters?.description) {
      query.andWhere('parcel.description like :description', {
        description: `%${filters?.description}%`,
      });
    }

    return await query.getMany();
  }

  async create(dto: CreateParcelDTO): Promise<Parcel> {
    const entity = this.repository.create(dto);
    await this.repository.insert(entity);
    this.eventEmitter.emit(
      ParcelAddedEvent.event,
      new ParcelAddedEvent(entity),
    );
    return entity;
  }
}
