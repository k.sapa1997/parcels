import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  MinLength,
  MaxLength,
  IsNotEmpty,
  IsDateString,
} from 'class-validator';
import { Parcel } from './parcel.entity';

export class CreateParcelDTO {
  @ApiProperty({ example: '123456123456' })
  @MinLength(8)
  @MaxLength(12)
  SKU: string;

  @ApiProperty({ example: 'Books from amazon' })
  @IsNotEmpty()
  description: string;

  @ApiProperty({ example: 'Estonia' })
  @IsNotEmpty()
  country: string;

  @ApiProperty({ example: 'Tallinn' })
  @IsNotEmpty()
  town: string;

  @ApiProperty({ example: 'Ehitajate tee 5' })
  @IsNotEmpty()
  street: string;

  @ApiProperty({ example: '2023-01-07T14:15:49.395Z' })
  @IsDateString()
  deliveryDate: Date;
}

export class ParcelDto extends CreateParcelDTO {
  @ApiProperty({ example: 123 })
  id: number;

  @ApiProperty({ example: '2023-01-07T14:15:49.395Z' })
  createdAt: Date;

  @ApiProperty({ example: '2023-01-07T14:15:49.395Z' })
  updatedAt: Date;

  constructor(
    id: number,
    createdAt: Date,
    updatedAt: Date,
    SKU: string,
    description: string,
    country: string,
    town: string,
    street: string,
    deliveryDate: Date,
  ) {
    super();
    this.id = id;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.SKU = SKU;
    this.description = description;
    this.country = country;
    this.town = town;
    this.street = street;
    this.deliveryDate = deliveryDate;
  }

  static fromEntity(entity: Parcel) {
    return new ParcelDto(
      entity.id,
      entity.createdAt,
      entity.updatedAt,
      entity.SKU,
      entity.description,
      entity.country,
      entity.town,
      entity.street,
      entity.deliveryDate,
    );
  }
}

export class GetAllQueryDTO {
  @ApiPropertyOptional({ example: 'Estonia' })
  country?: string;

  @ApiPropertyOptional({ example: 'part of description' })
  description?: string;
}
