import { Module } from '@nestjs/common';
import { ParcelModule } from './parcel/parcel.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Parcel } from './parcel/parcel.entity';
import { CountriesModule } from './countries/countries.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { Country } from './countries/country.entity';

@Module({
  imports: [
    EventEmitterModule.forRoot(),
    ParcelModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'database',
      port: 5432,
      username: 'postgres',
      password: 'postgres',
      database: 'postgres',
      entities: [Parcel, Country],
      synchronize: true,
    }),
    CountriesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
