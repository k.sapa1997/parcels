import { Parcel } from '../parcel/parcel.entity';

export class ParcelAddedEvent {
  static event: 'parcel.created';
  parcel: Parcel;

  constructor(parcel: Parcel) {
    this.parcel = parcel;
  }
}
