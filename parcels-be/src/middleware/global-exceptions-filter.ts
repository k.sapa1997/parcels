import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { Response } from 'express';
import { TypeORMError } from 'typeorm';

@Catch()
export class GlobalExceptionHandler implements ExceptionFilter {
  private readonly logger = new Logger(GlobalExceptionHandler.name);

  catch(exception: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    if (exception instanceof HttpException) {
      this.logger.warn(exception.getResponse());
      return response
        .status(exception.getStatus())
        .json(exception.getResponse());
    }

    if (
      exception instanceof TypeORMError &&
      exception.message.includes('violates unique')
    ) {
      this.logger.warn({ message: exception.toString() });

      return response.status(HttpStatus.BAD_REQUEST).json({
        status: HttpStatus.BAD_REQUEST,
        message: exception['detail'],
      });
    }

    this.logger.error({
      message: `Unexpected error: ${exception.toString()}`,
    });
    return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      message: 'Unknown error occurred',
    });
  }
}
