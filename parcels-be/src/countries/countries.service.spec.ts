import { Test, TestingModule } from '@nestjs/testing';
import { CountriesService } from './countries.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Country } from './country.entity';

const mockedRepo = {
  find: jest.fn(),
  create: jest.fn(),
  insert: jest.fn(),
};

describe('CountriesService', () => {
  let service: CountriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CountriesService,
        {
          provide: getRepositoryToken(Country),
          useValue: mockedRepo,
        },
      ],
    }).compile();

    service = module.get<CountriesService>(CountriesService);
    mockedRepo.find.mockClear();
    mockedRepo.create.mockClear();
    mockedRepo.insert.mockClear();
  });

  describe('.registerCountry', () => {
    it('should register new country if not exist yet', async () => {
      mockedRepo.find.mockResolvedValue([{ name: 'Estonia' }]);
      await service.registerCountry('USA');

      expect(mockedRepo.create).toHaveBeenCalledTimes(1);
      expect(mockedRepo.insert).toHaveBeenCalledTimes(1);
    });
    it('should not register country if already present', async () => {
      mockedRepo.find.mockResolvedValue([{ name: 'Estonia' }]);
      await service.registerCountry('Estonia');

      expect(mockedRepo.create).not.toHaveBeenCalled();
      expect(mockedRepo.insert).not.toHaveBeenCalled();
    });
  });

  describe('.getAllCountryNames', () => {
    it('should return mapped names of all found countries', async () => {
      mockedRepo.find.mockResolvedValue([
        { name: 'Estonia' },
        { name: 'USA' },
        { name: 'Israel' },
      ]);

      const res = await service.getAllCountryNames();

      expect(res).toEqual(['Estonia', 'USA', 'Israel']);
    });
  });
});
