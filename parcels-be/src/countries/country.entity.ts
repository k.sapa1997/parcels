import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../abstract/base.entity';

@Entity('countries')
export class Country extends BaseEntity {
  @Column({ unique: true })
  name: string;
}
