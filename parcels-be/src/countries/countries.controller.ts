import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CountriesService } from './countries.service';
import { OnEvent } from '@nestjs/event-emitter';
import { ParcelAddedEvent } from '../events/parcel-added.event';

@Controller('countries')
@ApiTags('countries')
export class CountriesController {
  constructor(private service: CountriesService) {}

  @Get()
  @ApiOkResponse({ isArray: true })
  async getAllNames(): Promise<string[]> {
    return this.service.getAllCountryNames();
  }

  @OnEvent(ParcelAddedEvent.event)
  async handleOrderCreatedEvent(payload: ParcelAddedEvent) {
    await this.service.registerCountry(payload.parcel.country);
  }
}
