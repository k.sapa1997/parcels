import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Country } from './country.entity';

@Injectable()
export class CountriesService {
  constructor(
    @InjectRepository(Country)
    private countriesRepository: Repository<Country>,
  ) {}
  async registerCountry(country: string) {
    const existing = await this.getAllCountryNames();
    if (existing.includes(country)) {
      return;
    }

    const newEntity = this.countriesRepository.create({ name: country });
    await this.countriesRepository.insert(newEntity);
  }

  async getAllCountryNames(): Promise<string[]> {
    const res = await this.countriesRepository.find();
    return res.map((entity) => entity.name);
  }
}
