## How to run
Assuming there's docker on your machine, just run `docker-compose up`

## Important notes

- I haven't used angular at all since my first software engineering job, therefore I am probably quite outdated on best practises, patterns etc. In case of joining a company I would of course take some time to refresh knowledge and learn modern ways of writing Angular.
- In case of real assignment, I would clarify few things with PM beforehand, here I took some assumptions (mostly country selection) that may be incorrect:
  - Country is not ENUM field pre-programmed before, but just a random string
  - You can insert parcel with country which didn't exist in system before and this process adds this country to autocomplete input for future searches
    - Countries are case-sensitive here, while it's easily fixable by lower-casing them before inserting to DB
- I haven't written many tests due to it being test assignment, in real project probably more test would have present
