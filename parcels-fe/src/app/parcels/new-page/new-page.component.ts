import { Component } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { DatePipe, NgForOf, NgIf } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule } from '@angular/material/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { CountryPickerComponent } from '../../shared/country-picker/country-picker.component';
import { Parcel } from '../parcel';
import { ParcelsService } from '../../gen/api/v1';

@Component({
  selector: 'list-page',
  imports: [
    RouterLink,
    MatTableModule,
    DatePipe,
    MatButtonModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    NgIf,
    CountryPickerComponent,
    NgForOf,
  ],
  standalone: true,
  templateUrl: './new-page.component.html',
})
export class NewPageComponent {
  constructor(private parcelService: ParcelsService, private router: Router) {}

  errors: string[] = [];

  parcelForm = new FormGroup({
    sku: new FormControl<string>('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(12),
    ]),
    description: new FormControl<string>('', [Validators.required]),
    country: new FormControl<string>('', [Validators.required]),
    town: new FormControl<string>('', [Validators.required]),
    street: new FormControl<string>('', [Validators.required]),
    deliveryDate: new FormControl<Date>(new Date(), [Validators.required]),
  });

  onFormSubmit() {
    const parcel: Parcel = {
      SKU: this.parcelForm.controls.sku.value!,
      description: this.parcelForm.controls.description.value!,
      country: this.parcelForm.controls.country.value!,
      town: this.parcelForm.controls.town.value!,
      street: this.parcelForm.controls.street.value!,
      deliveryDate: this.parcelForm.controls.deliveryDate.value!,
    };

    const res = this.parcelService.parcelControllerCreate({
      ...parcel,
      deliveryDate: parcel.deliveryDate.toISOString(),
    });

    res.subscribe({
      complete: async () => {
        await this.router.navigate(['/parcels']);
      },
      error: (error) => {
        const msg = error.error.message;
        this.errors = Array.isArray(msg) ? msg : [msg];
      },
    });
  }
}
