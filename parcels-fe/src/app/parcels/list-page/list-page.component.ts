import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { AsyncPipe, DatePipe, NgForOf } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { Parcel, ParcelFields } from '../parcel';
import { CountryPickerComponent } from '../../shared/country-picker/country-picker.component';
import { ParcelsService } from '../../gen/api/v1';

const countrySort = (a: Parcel, b: Parcel) => {
  if (a.country === b.country) {
    return 0;
  }
  if (a.country === 'Estonia') {
    return -1;
  }
  if (b.country === 'Estonia') {
    return 1;
  }

  return 0;
};

const sortParcels = (parcels: Parcel[]) => parcels.sort(
  (a, b) => countrySort(a, b) || a.deliveryDate.getTime() - b.deliveryDate.getTime(),
);

@Component({
  selector: 'list-page',
  imports: [
    RouterLink,
    MatTableModule,
    DatePipe,
    MatButtonModule,
    MatDividerModule,
    MatInputModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    NgForOf,
    CountryPickerComponent,
    AsyncPipe,
  ],
  standalone: true,
  templateUrl: './list-page.component.html',
})
export class ListPageComponent implements OnInit {
  constructor(private parcelService: ParcelsService) {}

  ngOnInit() {
    this.search();
  }

  parcels: Parcel[] = [];

  displayedColumns = ParcelFields;

  country = new FormControl<string>('');

  description = new FormControl<string>('');

  search() {
    this.parcelService
      .parcelControllerGetAll(
        this.country.value || '',
        this.description.value || '',
      )
      .subscribe((res) => {
        this.parcels = sortParcels(
          res.map((dto) => ({
            ...dto,
            deliveryDate: new Date(dto.deliveryDate),
          })),
        );
      });
  }
}
