export interface Parcel {
  SKU: string;
  description: string;
  country: string;
  town: string;
  street: string;
  deliveryDate: Date;
}

export const ParcelFields: (keyof Parcel)[] = [
  'SKU',
  'description',
  'country',
  'town',
  'street',
  'deliveryDate',
];
