import { Parcel } from './parcel';

const ALL_DATA: Parcel[] = [
  {
    SKU: '1234567890',
    description: 'Mock 1',
    country: 'USA',
    town: 'Tallinn',
    street: 'Some address',
    deliveryDate: new Date(),
  },
  {
    SKU: '1234567890',
    description: 'Mock 1',
    country: 'UK',
    town: 'Tallinn',
    street: 'Some address',
    deliveryDate: new Date('2022-12-17T03:24:00'),
  },
  {
    SKU: '1234567890',
    description: 'Mock 1',
    country: 'Estonia',
    town: 'Tallinn',
    street: 'Some address',
    deliveryDate: new Date(),
  },
  {
    SKU: '1234567890',
    description: 'More s 1',
    country: 'Estonia',
    town: 'Tallinn',
    street: 'Some address',
    deliveryDate: new Date('2010-12-17T03:24:00'),
  },
  {
    SKU: '1234567890',
    description: 'Mock 1',
    country: 'Estonia',
    town: 'Tallinn',
    street: 'Some address',
    deliveryDate: new Date('2022-12-18T03:24:00'),
  },
  {
    SKU: '1234567890',
    description: 'Mock 1',
    country: 'UK',
    town: 'Tallinn',
    street: 'Some address',
    deliveryDate: new Date(),
  },
];

export const MOCK_PARCELS_DATA = (
  country?: string | null,
  description?: string | null,
) => ALL_DATA.filter(
  (it) => (!country || it.country === country)
      && (!description || it.description.includes(description)),
);
