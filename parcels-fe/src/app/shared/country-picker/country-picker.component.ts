import { Component, Input } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { AsyncPipe, NgForOf } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { Observable } from 'rxjs';
import { CountriesService } from '../../gen/api/v1';

@Component({
  selector: 'country-picker',
  imports: [
    ReactiveFormsModule,
    MatAutocompleteModule,
    NgForOf,
    MatInputModule,
    AsyncPipe,
  ],
  standalone: true,
  templateUrl: './country-picker.component.html',
})
export class CountryPickerComponent {
  constructor(private countryService: CountriesService) {}

  @Input() country!: FormControl;

  countries$: Observable<string[]> = this.countryService.countriesControllerGetAllNames();
}
