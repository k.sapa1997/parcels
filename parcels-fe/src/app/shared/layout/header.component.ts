import { Component } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'layout-header',
  imports: [
    MatToolbarModule,
    RouterLink,
  ],
  standalone: true,
  templateUrl: './header.component.html',
})
export class HeaderComponent {
}
