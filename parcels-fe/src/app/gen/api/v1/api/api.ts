export * from './countries.service';
import { CountriesService } from './countries.service';
export * from './parcels.service';
import { ParcelsService } from './parcels.service';
export const APIS = [CountriesService, ParcelsService];
