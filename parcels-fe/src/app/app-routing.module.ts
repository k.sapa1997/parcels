import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListPageComponent } from './parcels/list-page/list-page.component';
import { NewPageComponent } from './parcels/new-page/new-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/parcels', pathMatch: 'full' },
  { path: 'parcels', component: ListPageComponent },
  { path: 'parcels/new', component: NewPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {

}
